type expression =
      Variable of string
    | Constant of int
    | Add of expression * expression
    | Substract of expression * expression
    | Mult of expression * expression
    | Divide of expression * expression

let rec string_of_expr e =
    match e with
    | Variable v -> v
    | Constant n -> string_of_int n
    | Add(e1,e2) -> "(" ^ string_of_expr e1 ^ "+" ^ string_of_expr e2 ^ ")"
    | Substract(e1,e2) -> "(" ^ string_of_expr e1 ^ "-" ^ string_of_expr e2 ^ ")"
    | Mult(e1,e2) -> "(" ^ string_of_expr e1 ^ "*" ^ string_of_expr e2 ^ ")"
    | Divide(e1,e2) -> "(" ^ string_of_expr e1 ^ "/" ^ string_of_expr e2 ^ ")"


