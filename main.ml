open React
open Lwt
open LTerm_text

let parse_and_eval s =
    let lexbuf = Sedlexing.Latin1.from_string s in
    try
        let lexemes = Lexer.lexer lexbuf in
        let e = Parser.parser lexemes in

        let se = "Lu : " ^ Expr.string_of_expr e ^ "\n" in

        let res = Eval.eval e in
        let sres = Printf.sprintf "Résultat : %d" res in

        se ^ sres
    with 
        | Lexer.LexingError -> "Lexing error"
        | Parser.ParsingError -> "Parsing error"
        | Eval.UnboundError -> "Unbound variable"
    

class read_line ~term ~history =
  object (self)
    inherit LTerm_read_line.read_line ~history ()
    inherit [Zed_string.t] LTerm_read_line.term term
    method! show_box = false

    initializer self#set_prompt (S.const (eval [ S "# " ]))
  end

let rec loop term history =
  Lwt.catch
    (fun () ->
      let rl =
        new read_line ~term ~history:(LTerm_history.contents history)
      in

      rl#run >|= fun command -> Some command)
    (function Sys.Break -> return None | exn -> Lwt.fail exn)
  >>= function
  | Some entry ->
      let entry_u8  = Zed_string.to_utf8 entry in
      let out = parse_and_eval entry_u8 in
      LTerm.fprintls term (eval [ S out ]) >>= fun () ->
      LTerm_history.add history entry;
      loop term history 
  | None -> loop term history 

let main () =
  LTerm_inputrc.load () >>= fun () ->
  Lwt.catch
    (fun () ->
      Lazy.force LTerm.stdout >>= fun term ->
      loop term (LTerm_history.create []))
    (function
      | LTerm_read_line.Interrupt -> Lwt.return () | exn -> Lwt.fail exn)

let () =
  Lwt_main.run (main ())

