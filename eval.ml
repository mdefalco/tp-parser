exception UnboundError

let rec eval e =
    let open Expr in
    match e with
    | Variable _ -> raise UnboundError
    | Constant n -> n
    | Add(e1,e2) -> eval e1 + eval e2
    | Substract(e1,e2) -> eval e1 - eval e2
    | Mult(e1,e2) -> eval e1 * eval e2
    | Divide(e1,e2) -> eval e1 / eval e2
