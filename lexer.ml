type token =
      Id of string
    | Num of int
    | Plus | Times | Minus | Div
    | LPar | RPar

exception LexingError

let rec lexer buf =
    match%sedlex buf with
    | ('a'..'z'|'A'..'Z'), Star('a'..'z'|'A'..'Z'|'0'..'9') ->
        let s = Sedlexing.Latin1.lexeme buf in
        Id s :: lexer buf
    | ('0' | ('1'..'9',Star('0'..'9'))) ->
        let s = Sedlexing.Latin1.lexeme buf in
        Num (int_of_string s) :: lexer buf
    | '+' -> Plus :: lexer buf
    | '*' -> Times :: lexer buf
    | '-' -> Minus :: lexer buf
    | '/' -> Div :: lexer buf
    | '(' -> LPar :: lexer buf
    | ')' -> RPar :: lexer buf
    | white_space -> lexer buf
    | eof -> []
    | _ -> raise LexingError


