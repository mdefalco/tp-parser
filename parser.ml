exception ParsingError

let parser tl = 
    let open Lexer in
    let open Expr in
    let rec parse_factor tl =
        match tl with
        | Id x :: q -> (Variable x, q)
        | Num n :: q -> (Constant n, q)
        | LPar :: q -> 
            begin
            match parse_expression q with
            | e, RPar::q -> (e,q)
            | _ -> raise ParsingError
            end
        | _ -> raise ParsingError
    and parse_term tl =
        match parse_factor tl with
        | (f, Times :: q) ->
            let t, q  = parse_term q in
            Mult(f, t), q
        | (f, Div :: q) ->
            let t, q  = parse_term q in
            Divide(f, t), q
        | (f, q) -> (f, q)
    and parse_expression tl =
        match parse_term tl with
        | (t, Plus :: q) ->
            let e, q = parse_expression q in
            Add(t, e), q
        | (t, Minus :: q) ->
            let e, q = parse_expression q in
            Substract(t, e), q
        | (t, q) -> (t, q)
    in

    let e, q = parse_expression tl in

    if q <> []
    then raise ParsingError;
    e

